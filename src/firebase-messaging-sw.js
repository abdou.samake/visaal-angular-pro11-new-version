
`
<script type="module">
  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.4/firebase-app.js";
  import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.8.4/firebase-analytics.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  const firebaseConfig = {
  apiKey: "AIzaSyAEL190iINnKjVjCxkabdk6Ye6rpYC-S7o",
  authDomain: "visaal-fc520.firebaseapp.com",
  databaseURL: "https://visaal-fc520.firebaseio.com",
  projectId: "visaal-fc520",
  storageBucket: "visaal-fc520.appspot.com",
  messagingSenderId: "1099005813493",
  appId: "1:1099005813493:web:f505f9775f3e9e3a381bbc",
  measurementId: "G-Z7TTNQPLYL"
};

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const analytics = getAnalytics(app);
  const messagerie = firebase.messaging();
</script>
  `
