import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditOmsDataComponent } from './edit-oms-data.component';

describe('EditOmsDataComponent', () => {
  let component: EditOmsDataComponent;
  let fixture: ComponentFixture<EditOmsDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditOmsDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditOmsDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
