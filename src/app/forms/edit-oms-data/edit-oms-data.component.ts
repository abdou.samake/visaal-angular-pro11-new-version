import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {OmsDataService} from '../../services/oms-data.service';
import {ActivatedRoute, Router} from '@angular/router';
import {PatientsService} from '../../services/patients.service';
import {OmsData} from '../../models/oms-data';
import {Subject} from 'rxjs';
import {takeUntil, tap} from 'rxjs/operators';

@Component({
  selector: 'app-edit-oms-data',
  templateUrl: './edit-oms-data.component.html',
  styleUrls: ['./edit-oms-data.component.scss']
})
export class EditOmsDataComponent implements OnInit, OnDestroy {

  destroy$: Subject<boolean> = new Subject();
  public formEdit: FormGroup = Object.create(null);
  volumeUrine: string | undefined;
  frequenceUrine: string  | undefined;

  patient: any;


  constructor(private fb: FormBuilder, private omsDataService: OmsDataService,
              private route: ActivatedRoute,
              private router: Router,
              private patientService: PatientsService) { }

  ngOnInit(): void {
    this.initForm();
    this.patientService.getAllPatients$()
      .pipe(
        takeUntil(this.destroy$),
        tap(
          patients => {
            patients.forEach(patient => this.patient = patient);
          }
        )
      )
      .subscribe();

    const id = this.route.snapshot.params.id;
    console.log(id);
    this.omsDataService.getOmsById$(id)
      .pipe(
        takeUntil(this.destroy$),
        tap((data: OmsData) => {
          this.volumeUrine = data.volume;
        })
      )
      .subscribe()
    ;
  }

  initForm(): void {
    this.formEdit = this.fb.group({
      volume: ['', Validators.required]
    });
  }

  onSubmit(): void {
    const id = this.route.snapshot.params.id;
    this.omsDataService.updateOmsDataById$(this.formEdit.value, id)
      .pipe(
        takeUntil(this.destroy$),
        tap(() => {
          this.router.navigate(['tables/footerrow-table']);
        })
      )
      .subscribe();

  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
