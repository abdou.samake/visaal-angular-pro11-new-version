import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFireStorage} from '@angular/fire/storage';
import {Router} from '@angular/router';
import {AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {

  currentUser: any;
  message: string | undefined;
  errorMsg: string | undefined;

  public form: FormGroup = Object.create(null);
  constructor(private auth: AngularFireAuth, private fb: FormBuilder, private afs: AngularFirestore,
              private storage: AngularFireStorage, private router: Router) {
    this.auth.authState.subscribe((user) => this.currentUser = user);
  }

  ngOnInit(): void {
    this._initForm();
  }

  _initForm(): void {
    this.form = this.fb.group({
      gender: [null, [Validators.required]],
      firstName: [null, [Validators.required]],
      lastName: [null, [Validators.required]],
      zip: [null, [Validators.required]],
      birthday: [null, [Validators.required]],
      adress: [null, Validators.required],
      country: [null, [Validators.required]],
      city: [null, [Validators.required]],
      role: [null, [Validators.required]],
    });
  }

  onSubmit(): void {
      this.setUserDataForm()
        .then(
            (data: any) => {
            console.log(data);
            this.message = 'mise à jour résussit';
            this.form.reset();
            this.router.navigateByUrl('/pages/user-profile');
          })
        .catch((err: any) => this.errorMsg = err);
  }

  // tslint:disable-next-line:typedef
  setUserDataForm() {
    const userRef: AngularFirestoreDocument<any> = this.afs.collection('users').doc(this.currentUser.uid);
    const userData: { zip: any; birthday: any; uid: any; firstName: any; lastName: any; country: any; role: any; city: any; adress: any; email: any} = {
      uid: this.currentUser.uid,
      email: this.currentUser.email,
      firstName: this.form.value.firstName,
      lastName: this.form.value.lastName,
      zip: this.form.value.zip,
      birthday: this.form.value.birthday,
      adress: this.form.value.adress,
      country: this.form.value.country,
      city: this.form.value.city,
      role: this.form.value.role,
    };
    return userRef.set(userData, {merge: true});
  }
}
