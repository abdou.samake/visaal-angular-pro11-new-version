import {Component, OnDestroy, OnInit} from '@angular/core';
import {DeviceModel} from '../../models/devices';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PatientsService} from '../../services/patients.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DevicesService} from '../../services/devices.service';
import {Subject} from 'rxjs';
import {takeUntil, tap} from 'rxjs/operators';
import {ConfirmationDialogComponent} from '../../shared/confirmation-dialog/confirmation-dialog.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-edit-patient-dm2',
  templateUrl: './edit-patient-dm2.component.html',
  styleUrls: ['./edit-patient-dm2.component.scss']
})
export class EditPatientDM2Component implements OnInit, OnDestroy {

  destroy$: Subject<boolean> = new Subject();

  message: string = 'null';

  submitted = false;
  patient: any;
  lastName: any;
  firstName: any;
  sex: any;
  dateOfBirthday: any;
  service: any;
  numberOfSecu: any;
  numberOfDevice: any;
  numberOfBed: any;
  floor: any;
  numberOfRoom: any;


  devices: Array<DeviceModel> | undefined;

  public form: FormGroup = Object.create(null);

  constructor(private fb: FormBuilder, private patientsService: PatientsService,
              private router: Router,
              private deviceService: DevicesService,
              private route: ActivatedRoute,
              public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    const id = this.route.snapshot.params.id;

    this.form = this.fb.group({
      lastName: [null, Validators.compose([Validators.required])
      ],
      firstName: [
        null, Validators.compose([Validators.required])
      ],
      numberOfSecu: [null, Validators.compose([Validators.required])
      ],
      service: [null, Validators.compose([Validators.required])
      ],
      floor: [
        null,
        Validators.compose([Validators.required, Validators.minLength(-1)])
      ],
      numberOfDevice: [
        null,
        Validators.compose([Validators.required])
      ],
      numberOfRoom: [
        null,
        Validators.compose([Validators.required, Validators.minLength(1)])
      ],
      numberOfBed: [
        null,
        Validators.compose([Validators.required, Validators.minLength(1)])
      ],
      dateOfBirthday: [
        null,
        Validators.compose([Validators.required])
      ],
      sex: [null, Validators.required]
    });

    this.deviceService.getAllDevices$().subscribe(res => this.devices = res);
    this.patientsService.getPatientUseDM2ById$(id)
      .pipe(
        takeUntil(this.destroy$),
        tap(
          data => {
            this.patient = data;
            this.lastName = data.lastName;
            this.firstName = data.firstName;
            this.sex = data.sex;
            this.dateOfBirthday = data.dateOfBirthday;
            this.service = data.service;
            this.numberOfSecu = data.numberOfSecu;
            this.numberOfDevice = data.numberOfDevice;
            this.floor = data.floor;
            this.numberOfRoom = data.numberOfRoom;
            this.numberOfBed = data.numberOfBed;
          }
        )
      ).subscribe();
  }

  /*onSubmit(): void {
    this.patientsService.updatePatientDM2ById$(this.form.value, this.patient.id)
      .pipe(
        takeUntil(this.destroy$),
        tap(
          () => {
            this.router.navigate(['/tables/footerrow-table']);
          }
        )
      ).subscribe();
  }*/

  onSubmit(): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: 'Avant de continuer cet action veuillez vous assurer que les données du précédent ' +
        'patient ont été bien ' +
        'télécharger en pdf. Voulez vous vraiment continuer cet action ?'
    });
    dialogRef.afterClosed()
      .pipe(
        takeUntil(this.destroy$),
        tap(
          result => {
            if (result) {
              console.log('Yes clicked');
              this.patientsService.updatePatientDM2ById$(this.form.value, this.patient.id)
                .pipe(
                  takeUntil(this.destroy$),
                );
            }
          },
          () => this.message = 'attribution réussie !',
        ),
        tap(() => this.router.navigate(['/tables/footerrow-table']))
      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
