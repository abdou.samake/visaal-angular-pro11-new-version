import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPatientDM2Component } from './edit-patient-dm2.component';

describe('EditPatientDM2Component', () => {
  let component: EditPatientDM2Component;
  let fixture: ComponentFixture<EditPatientDM2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditPatientDM2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPatientDM2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
