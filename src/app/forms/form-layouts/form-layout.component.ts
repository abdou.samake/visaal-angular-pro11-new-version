import {Component, OnDestroy, OnInit} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FormControl, Validators } from '@angular/forms';
import {DeviceModel} from '../../models/devices';
import {PatientsService} from '../../services/patients.service';
import {Router} from '@angular/router';
import {DevicesService} from '../../services/devices.service';
import {Subject} from 'rxjs';
import {tap} from 'rxjs/operators';

@Component({
  selector: 'app-formlayout',
  templateUrl: './form-layout.component.html',
  styleUrls: ['./form-layout.component.scss']
})
export class FormLayoutComponent implements OnInit, OnDestroy{

  destroy$: Subject<boolean> = new Subject();
  submitted = false;
  numbers = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'VIIII', 'X'];
  devices: Array<DeviceModel> | undefined;

  public form: FormGroup = Object.create(null);

  constructor(private fb: FormBuilder, private patientsService: PatientsService,
              private router: Router, private deviceService: DevicesService) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      service: [null, Validators.compose([Validators.required])
      ],
      number: [
        null, Validators.compose([Validators.required])
      ],
      status: [null, Validators.compose([Validators.required])
      ]

    });

    this.deviceService.getAllDevices$()
      .pipe(
        tap(res => this.devices = res)
      )
      .subscribe();
  }


  onSubmit(): void {

    this.deviceService.addDevice$(this.form.value)
      .then(
        data => {
          console.log(data);
          this.submitted = true;
          this.router.navigate(['/tables/basictable']);
        });
  }
  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
