import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPatientDM3Component } from './edit-patient-dm3.component';

describe('EditPatientDM3Component', () => {
  let component: EditPatientDM3Component;
  let fixture: ComponentFixture<EditPatientDM3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditPatientDM3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPatientDM3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
