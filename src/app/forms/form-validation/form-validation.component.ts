import {Component, OnDestroy, OnInit} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { CustomValidators } from 'ngx-custom-validators';
import {PatientsService} from '../../services/patients.service';
import {Router} from '@angular/router';
import {DeviceModel} from '../../models/devices';
import {DevicesService} from '../../services/devices.service';
import {Subject, Subscription} from 'rxjs';
import {takeUntil, tap} from 'rxjs/operators';

const password = new FormControl('', Validators.required);
const confirmPassword = new FormControl('', CustomValidators.equalTo(password));

@Component({
  selector: 'app-form-validation',
  templateUrl: './form-validation.component.html',
  styleUrls: ['./form-validation.component.scss']
})
export class FormValidationComponent implements OnInit, OnDestroy{

  destroy$: Subject<boolean> = new Subject();

  submitted = false;

  devices: Array<DeviceModel> | undefined;

  public form: FormGroup = Object.create(null);

  constructor(private fb: FormBuilder, private patientsService: PatientsService, private router: Router, private deviceService: DevicesService) { }


  ngOnInit(): void {
    this.form = this.fb.group({
      lastName: [null, Validators.compose([Validators.required])
      ],
      firstName: [
        null, Validators.compose([Validators.required])
      ],
      numberOfSecu: [null, Validators.compose([Validators.required])
      ],
      service: [null, Validators.compose([Validators.required])
      ],
      floor: [
        null,
        Validators.compose([Validators.required, Validators.minLength(-1)])
      ],
      numberOfDevice: [
        null,
        Validators.compose([Validators.required])
      ],
      numberOfRoom: [
        null,
        Validators.compose([Validators.required, Validators.minLength(1)])
      ],
      numberOfBed: [
        null,
        Validators.compose([Validators.required, Validators.minLength(1)])
      ],
      dateOfBirthday: [
        null,
        Validators.compose([Validators.required])
      ],
      sex: [null, Validators.required]

    });

    this.deviceService.getAllDevices$()
      .pipe(
        takeUntil(this.destroy$),
        tap(
          res => this.devices = res
        )
      )
      .subscribe();
  }


  onSubmit(): void {

      this.patientsService.addPatient$(this.form.value)
        .then(
          data => {
            console.log(data);
            this.router.navigate(['tables/filterable']);
          });
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
