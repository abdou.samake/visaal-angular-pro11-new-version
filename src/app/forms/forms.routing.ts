import { Routes } from '@angular/router';

import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { RadiobuttonComponent } from './radiobutton/radiobutton.component';
import { FormfieldComponent } from './formfield/formfield.component';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { FormLayoutComponent } from './form-layouts/form-layout.component';
import { PaginatiorComponent } from './paginator/paginator.component';
import { SortheaderComponent } from './sortheader/sortheader.component';
import { SelectfieldComponent } from './select/select.component';
import { InputfieldComponent } from './input/input.component';
import { EditorComponent } from './editor/editor.component';
import { FormValidationComponent } from './form-validation/form-validation.component';
import { WizardComponent } from './wizard/wizard.component';
import { MultiselectComponent } from './multiselect/multiselect.component';
import {EditPatientComponent} from './edit-patient/edit-patient.component';
import {AddDeviceComponent} from './add-device/add-device.component';
import {EditProfileComponent} from './edit-profile/edit-profile.component';
import {EditOmsDataComponent} from './edit-oms-data/edit-oms-data.component';
import {EditPatientDM2Component} from './edit-patient-dm2/edit-patient-dm2.component';
import {EditPatientDM3Component} from './edit-patient-dm3/edit-patient-dm3.component';

export const FormRoutes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'autocomplete',
                component: AutocompleteComponent,
                data: {
                    title: 'Autocomplete',
                    urls: [
                        { title: 'Dashboard', url: '/dashboard' },
                        { title: 'Autocomplete' }
                    ]
                }
            },
            {
                path: 'checkbox',
                component: CheckboxComponent,
                data: {
                    title: 'Checkbox',
                    urls: [
                        { title: 'Dashboard', url: '/dashboard' },
                        { title: 'Checkbox' }
                    ]
                }
            },
            {
                path: 'radiobutton',
                component: RadiobuttonComponent,
                data: {
                    title: 'Radio Buttons',
                    urls: [
                        { title: 'Dashboard', url: '/dashboard' },
                        { title: 'Radio Buttons' }
                    ]
                }
            },
            {
                path: 'datepicker',
                component: DatepickerComponent,
                data: {
                    title: 'Datepicker',
                    urls: [
                        { title: 'Dashboard', url: '/dashboard' },
                        { title: 'Datepicker' }
                    ]
                }
            },
            {
                path: 'formfield',
                component: FormfieldComponent,
                data: {
                    title: 'Form Filed',
                    urls: [
                        { title: 'Dashboard', url: '/dashboard' },
                        { title: 'Form Filed' }
                    ]
                }
            },
            {
                path: 'input',
                component: InputfieldComponent,
                data: {
                    title: 'Input Field',
                    urls: [
                        { title: 'Dashboard', url: '/dashboard' },
                        { title: 'Input Field' }
                    ]
                }
            },
            {
                path: 'select',
                component: SelectfieldComponent,
                data: {
                    title: 'Select',
                    urls: [
                        { title: 'Dashboard', url: '/dashboard' },
                        { title: 'Select' }
                    ]
                }
            },
            {
                path: 'paginator',
                component: PaginatiorComponent,
                data: {
                    title: 'Paginator',
                    urls: [
                        { title: 'Dashboard', url: '/dashboard' },
                        { title: 'Paginator' }
                    ]
                }
            },
            {
                path: 'form-layout',
                component: FormLayoutComponent,
                data: {
                    title: 'Form Layout',
                    urls: [
                        { title: 'Dashboard', url: '/dashboard' },
                        { title: 'Form Layout' }
                    ]
                }
            },
            {
                path: 'editor',
                component: EditorComponent,
                data: {
                    title: 'Form Editor',
                    urls: [
                        { title: 'Dashboard', url: '/dashboard' },
                        { title: 'Form Editor' }
                    ]
                }
            },
            {
                path: 'form-validation',
                component: FormValidationComponent,
                data: {
                    title: 'Form Validation',
                    urls: [
                        { title: 'Dashboard', url: '/dashboard' },
                        { title: 'Form Validation' }
                    ]
                }
            },
          {
            path: 'form-add-device',
            component: AddDeviceComponent,
            data: {
              title: 'Add device',
              urls: [
                { title: 'Dashboard', url: '/dashboard' },
                { title: 'Add device' }
              ]
            }
          },
            {
                path: 'sortheader',
                component: SortheaderComponent,
                data: {
                    title: 'Sort Header',
                    urls: [
                        { title: 'Dashboard', url: '/dashboard' },
                        { title: 'Sort Header' }
                    ]
                }
            },
            {
                path: 'wizard',
                component: WizardComponent,
                data: {
                    title: 'Form Wizard',
                    urls: [
                        { title: 'Dashboard', url: '/dashboard' },
                        { title: 'Form Wizard' }
                    ]
                }
            },
            {
                path: 'multiselect',
                component: MultiselectComponent,
                data: {
                    title: 'Multiselect',
                    urls: [
                        { title: 'Dashboard', url: '/dashboard' },
                        { title: 'Multiselect' }
                    ]
                }
            },
          {
            path: 'edit-patient/:id',
            component: EditPatientComponent,
          },
          {
            path: 'edit-profile',
            component: EditProfileComponent
          },
          {
            path: 'edit-omsData/:id',
            component: EditOmsDataComponent
          },
          {
            path: 'edit-patient-DM2/:id',
            component: EditPatientDM2Component
          },
          {
            path: 'edit-patient-DM3/:id',
            component: EditPatientDM3Component
          }
        ]
    }
];
