
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../demo-material-module';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormRoutes } from './forms.routing';
import { QuillModule } from 'ngx-quill';

import { MatDatepickerModule } from '@angular/material/datepicker';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import { CheckboxComponent } from './checkbox/checkbox.component';
import { RadiobuttonComponent } from './radiobutton/radiobutton.component';
import { FormfieldComponent } from './formfield/formfield.component';
import { InputfieldComponent } from './input/input.component';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { FormLayoutComponent } from './form-layouts/form-layout.component';
import { PaginatiorComponent } from './paginator/paginator.component';
import { SortheaderComponent } from './sortheader/sortheader.component';
import { SelectfieldComponent } from './select/select.component';
import { EditorComponent } from './editor/editor.component';
import { FormValidationComponent } from './form-validation/form-validation.component';
import { UploadComponent } from './file-upload/upload.component';
import { WizardComponent } from './wizard/wizard.component';
import { MultiselectComponent } from './multiselect/multiselect.component';
import { EditPatientComponent } from './edit-patient/edit-patient.component';
import { AddDeviceComponent } from './add-device/add-device.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { EditOmsDataComponent } from './edit-oms-data/edit-oms-data.component';
import { EditPatientDM2Component } from './edit-patient-dm2/edit-patient-dm2.component';
import { EditPatientDM3Component } from './edit-patient-dm3/edit-patient-dm3.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(FormRoutes),
        DemoMaterialModule,
        FlexLayoutModule,
        FormsModule,
        QuillModule.forRoot(),
        ReactiveFormsModule,
        MatDatepickerModule,
        NgMultiSelectDropDownModule.forRoot()
    ],
    declarations: [
        AutocompleteComponent,
        CheckboxComponent,
        RadiobuttonComponent,
        FormfieldComponent,
        DatepickerComponent,
        FormLayoutComponent,
        InputfieldComponent,
        SortheaderComponent,
        SelectfieldComponent,
        EditorComponent,
        PaginatiorComponent,
        FormValidationComponent,
        UploadComponent,
        WizardComponent,
        MultiselectComponent,
        EditPatientComponent,
        AddDeviceComponent,
        EditProfileComponent,
        EditOmsDataComponent,
        EditPatientDM2Component,
        EditPatientDM3Component
    ]
})
export class FormModule { }
