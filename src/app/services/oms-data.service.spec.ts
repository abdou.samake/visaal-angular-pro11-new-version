import { TestBed } from '@angular/core/testing';

import { OmsDataService } from './oms-data.service';

describe('OmsDataService', () => {
  let service: OmsDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OmsDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
