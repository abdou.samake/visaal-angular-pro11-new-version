import { Injectable } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {from, Observable} from 'rxjs';
import {OmsData} from '../models/oms-data';

@Injectable({
  providedIn: 'root'
})
export class OmsDataService {

  constructor(private afs: AngularFirestore) { }

  getOmsData$(): Observable<OmsData[]> {
    return this.afs.collection('omsData').valueChanges() as Observable<OmsData[]>;
  }

  getOmsById$(id: any): Observable<OmsData> {
    return this.afs.collection('omsData').doc(id).valueChanges() as Observable<OmsData>;
  }

  updateOmsDataById$(data: OmsData, id: string): Observable<any> {
    return from(this.afs.collection('omsData').doc(id)
      .update({...data})) as Observable<any>;
  }
}


