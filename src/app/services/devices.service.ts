import { Injectable } from '@angular/core';
import {from, Observable, ReplaySubject} from 'rxjs';
import {DeviceModel} from '../models/devices';
import {HttpClient} from '@angular/common/http';
import {AngularFirestore} from '@angular/fire/firestore';
import {PatientModel} from '../models/patients';
import firebase from 'firebase';
import DocumentReference = firebase.firestore.DocumentReference;
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DevicesService {
  devices: ReplaySubject<Array<DeviceModel>>;
  device: ReplaySubject<DeviceModel>;


  constructor(private http: HttpClient, private afs: AngularFirestore) {
    this.devices = new ReplaySubject<Array<DeviceModel>>();
    this.device = new ReplaySubject<DeviceModel>();
  }


  getDeviceById$(id: string): Observable<any> {
    return this.afs.collection('appareils').doc(id).valueChanges() as Observable<PatientModel>;
  }


  async addDevice$(device: any) {
    const deviceAdd = await this.afs
      .collection('appareils')
      .add(device);
    const createNewDevice = this.afs
      .collection('appareils')
      .doc(deviceAdd.id);
    await createNewDevice
      .set({...device, id: deviceAdd.id});
    return device;
  }


  deleteDevice$(device: any): Promise<any> {
    return (this.afs.collection('appareils').doc(device.id).delete());
  }


  updateDeviceById$(device: any): Observable<any> {
    return from(this.afs.collection('appareils').doc(device.id)
      .update({...device})) as Observable<any>;
  }


  switchAllDeviceStatus(deviceId: string | undefined, status: any) {
    return this.afs.collection('appareils').doc(deviceId)
      .set({status}, {merge: true});
  }


  getAllDevices(): void {
    this.afs.collection('appareils')
      .snapshotChanges()
      .pipe(
        map(actions => {
          return actions.map(a => {
            const device = a.payload.doc.data() as DeviceModel;
            device.id = a.payload.doc.id;
            return device;
          });
        })
      )
      .subscribe((devices: Array<DeviceModel>) => this.devices.next(devices));
  }
  getAllDevices$(): Observable<DeviceModel[]> {
    return this.afs.collection('appareils').valueChanges() as Observable<DeviceModel[]>;
  }
  getDeviceById(deviceId: string): Observable<DeviceModel> {
    return this.afs.collection('appareils').doc(deviceId).valueChanges() as Observable<DeviceModel>;
  }

  getDeiveById(deviceId: string) {
    return new Promise((res, rej) => {
      this.afs.collection('appareils')
        .doc(deviceId)
        .get()
        .pipe(
          map(data => {
            const device = data.data() as DeviceModel;
            device.id = data.id;
            return device;
          })
        )
        .subscribe((device: DeviceModel) => this.device.next(device));
    });
  }

  addDevice(device: DeviceModel) {
    return new Promise(
      (res, rej) => {
        this.afs.collection('appareils')
          .add(device)
          .then(() => res())
          .catch(err => rej(err));
      }
    );
  }

  addFilePdfToDeviceById(id: string, filepdfUrl: { filePdfUrl: string | undefined }) {
    return this.afs.collection('appareils').doc(id).set(filepdfUrl, {merge: true});
  }
}
