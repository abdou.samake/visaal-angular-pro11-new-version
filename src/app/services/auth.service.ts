import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
import {User} from '../models/users';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  token: BehaviorSubject<string>;

  constructor(private afAuth: AngularFireAuth, private afs: AngularFirestore, private router: Router) {
    // @ts-ignore
    this.token = new BehaviorSubject<string>(null);
  }


  signIn(email: string, password: string) {
    return new Promise(
      (res, rej) => {
        this.afAuth.signInWithEmailAndPassword(email, password)
          .then((currentUser: any) => {
            this.token.next(currentUser.user.refreshToken);
            res();
          })
          .catch(err => {

            if (err.code === 'auth/invalid-email') {
              rej('L\'adresse email est invalide.');
            }

            if (err.code === 'auth/user-disabled') {
              rej('L\'utilisateur a été banni.');
            }

            if (err.code === 'auth/user-not-found') {
              rej('L\'utilisateur n\'existe pas.');
            }

            if (err.code === 'auth/wrong-password') {
              rej('Le mot de passe est incorrect.');
            }

            rej('Une erreur est survenue.');
          });

      }
    );

  }


  signUp(newUser: User) {
    return new Promise(
      (res, rej) => {
        this.afAuth.createUserWithEmailAndPassword(newUser.email, newUser.password)
          .then((currentUser: any) => {
            this.token.next(currentUser.user.refreshToken);

            newUser.id = currentUser.user.uid;

            this.afs.collection('users')
              .doc(currentUser.user.uid)
              .set(newUser)
              .then(() => res());
          })
          .catch((err) => {


            if (err.code === 'auth/email-already-in-use') {
              rej('L\'adresse email est déjà utilisée.');
            }

            if (err.code === 'auth/invalid-email') {
              rej('L\'adresse email est invalide.');
            }

            if (err.code === 'auth/operation-not-allowed') {
              rej('Une erreur est survenue.');
            }

            if (err.code === 'auth/weak-password') {
              rej('Le mot de passe n\'est pas sécurisé.');
            }

            rej('Une erreur est survenue.');
          });

      });
  }


  SignOut() {
    return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['login']);
    });
  }

}
