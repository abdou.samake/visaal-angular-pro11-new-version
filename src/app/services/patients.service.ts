import { Injectable } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {PatientModel} from '../models/patients';
import {from, Observable} from 'rxjs';
import {UrinesModel} from '../models/urines';
import {map, tap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import firebase from 'firebase';
import FieldValue = firebase.firestore.FieldValue;
import {VolumeAbout24h} from '../models/volume-about24h';


@Injectable({
  providedIn: 'root'
})
export class PatientsService {

  patient: PatientModel | undefined;
  constructor(private afs: AngularFirestore) {
  }

  getAllPatients$(): Observable<PatientModel[]> {
    return this.afs.collection('patients').valueChanges() as Observable<PatientModel[]>;
  }

  getAllPatientUseDM1$(): Observable<PatientModel[]> {
    return this.afs.collection('usersDM1').valueChanges() as Observable<PatientModel[]>;
  }

  getAllPatientUseDM2$(): Observable<PatientModel[]> {
    return this.afs.collection('usersDM2').valueChanges() as Observable<PatientModel[]>;
  }

  getAllPatientUseDM3$(): Observable<PatientModel[]> {
    return this.afs.collection('usersDM3').valueChanges() as Observable<PatientModel[]>;
  }

  getPatientById$(id: any): Observable<PatientModel> {
    return this.afs.collection('patients').doc(id).valueChanges() as Observable<PatientModel>;
  }

  getPatientUseDM1ById$(id: any): Observable<PatientModel> {
    return this.afs.collection('usersDM1').doc(id).valueChanges() as Observable<PatientModel>;
  }

  getPatientUseDM2ById$(id: any): Observable<PatientModel> {
    return this.afs.collection('usersDM2').doc(id).valueChanges() as Observable<PatientModel>;
  }

  getPatientUseDM3ById$(id: any): Observable<PatientModel> {
    return this.afs.collection('usersDM3').doc(id).valueChanges() as Observable<PatientModel>;
  }

  getAllPatientsFiles$(): Observable<any[]> {
    return this.afs.collection('archives').valueChanges() as Observable<any[]>;
  }

  getById(patientId: string | undefined) {

    return new Promise(
      (res, rej) => {
        this.afs
          .collection('patients')
          .doc(patientId)
          .get()
          .pipe(
            map(data => {
              const patient = data.data() as PatientModel;
              patient.id = data.id;
              return patient;
            })
          )
          .subscribe((patient: PatientModel) => res(patient));
      }
    );
  }

  // tslint:disable-next-line:typedef
  async addPatient$(patient: PatientModel) {
    const patientAdd = await this.afs.collection('patients').add(patient);
    const createNewPatient = this.afs.collection('patients').doc(patientAdd.id);
    await createNewPatient.set({...patient, id: patientAdd.id});
    return patient;
  }

  /*********************uPDATE Patient Use DM1********************************/

  updatePatientById$(patient: PatientModel, id: string): Observable<any> {
    return from(this.afs.collection('usersDM1').doc(id)
      .set({id,
        lastName: patient.lastName,
        firstName: patient.firstName,
        dateOfBirthday: patient.dateOfBirthday,
        sex: patient.sex,
        service: patient.service,
        numberOfRoom: patient.numberOfRoom,
        numberOfDevice: patient.numberOfDevice,
        numberOfBed: patient.numberOfBed
      })) as Observable<any>;
  }

  /*********************uPDATE Patient Use DM2********************************/
  updatePatientDM2ById$(patient: PatientModel, id: string): Observable<any> {
    return from(this.afs.collection('usersDM2').doc(id)
      .set({id,
        lastName: patient.lastName,
        firstName: patient.firstName,
        dateOfBirthday: patient.dateOfBirthday,
        sex: patient.sex,
        service: patient.service,
        numberOfRoom: patient.numberOfRoom,
        numberOfDevice: patient.numberOfDevice,
        numberOfBed: patient.numberOfBed
      })) as Observable<any>;
  }

  /*********************uPDATE Patient Use DM3********************************/
  updatePatientDM3ById$(patient: PatientModel, id: string): Observable<any> {
    return from(this.afs.collection('usersDM3').doc(id)
      .set({id,
        lastName: patient.lastName,
        firstName: patient.firstName,
        dateOfBirthday: patient.dateOfBirthday,
        sex: patient.sex,
        service: patient.service,
        numberOfRoom: patient.numberOfRoom,
        numberOfDevice: patient.numberOfDevice,
        numberOfBed: patient.numberOfBed,
        numberOfSecu: patient.numberOfSecu,
        urinesDM: patient.urinesDM
      })) as Observable<any>;
  }

  getUrineDataUseDMById(id: string): Observable<any> {
    return this.afs.collection('urinesDM').doc(id).valueChanges() as Observable<UrinesModel>;
  }

  getUrineH24DataUseDMById(id: string): Observable<any> {
    return this.afs.collection('urinesH24DM').doc(id).valueChanges() as Observable<UrinesModel>;
  }
  getvolumeAndDateOfDayByIdDM1(id: string): Observable<VolumeAbout24h> {
    return this.afs.collection('urines24hDM1').doc(id).valueChanges() as Observable<VolumeAbout24h>;
  }

  getvolumeAndDateOfDayByIdDM2(id: string): Observable<VolumeAbout24h> {
    return this.afs.collection('urines24hDM2').doc(id).valueChanges() as Observable<VolumeAbout24h>;
  }

  getvolumeAndDateOfDayByIdDM3(id: string): Observable<VolumeAbout24h> {
    return this.afs.collection('urines24hDM3').doc(id).valueChanges() as Observable<VolumeAbout24h>;
  }
  // tslint:disable-next-line:typedef
  edit(patient: PatientModel) {
        this.afs
          .collection('patients')
          .doc(patient.id)
          .update(
            {
              lastName: patient.lastName,
              firstName: patient.firstName,
              sex: patient.sex,
              service: patient.service,
              dateOfBirthday: patient.dateOfBirthday,
              numberOfSecu: patient.numberOfSecu,
              numberOfDevice: patient.numberOfDevice,
              floor: patient.floor,
              numberOfBed: patient.numberOfBed
            });
  }

  addFile(file: any): Promise<any> {
    return this.afs.collection('archives').add(file);
  }

  updateVolumeDM1ForWhich24hours(volumeUrine: number, dateOfDate: string, currentNbrUrine: number): void {
    this.afs.collection('urines24hDM1').doc('CZaujeT0XivL9pJ6r3xM').set({volume: volumeUrine, date: dateOfDate, nbrUrine: currentNbrUrine}, {merge: true});
  }

  updateVolumeDM2ForWhich24hours(volumeUrine: number, dateOfDate: string, currentNbrUrine: number): void {
    this.afs.collection('urines24hDM2').doc('7gPqdrM1bCB469m4dtiD').set({volume: volumeUrine, date: dateOfDate, nbrUrine: currentNbrUrine}, {merge: true});
  }

  updateVolumeDM3ForWhich24hours(volumeUrine: number, dateOfDate: string, currentNbrUrine: number): void {
    this.afs.collection('urines24hDM3').doc('R32fQzBOQYSKwkubFWGx').set({volume: volumeUrine, date: dateOfDate, nbrUrine: currentNbrUrine}, {merge: true});
  }

  getVolumeAndDateDM1ForWhich24Hours(): Observable<any> {
    return this.afs.collection('urines24hDM1').valueChanges() as Observable<any>;
  }

  getVolumeAndDateDM2ForWhich24Hours(): Observable<any> {
    return this.afs.collection('urines24hDM2').valueChanges() as Observable<any>;
  }

  getVolumeAndDateDM3ForWhich24Hours(): Observable<any> {
    return this.afs.collection('urines24hDM3').valueChanges() as Observable<any>;
  }

}
