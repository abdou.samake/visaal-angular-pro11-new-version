import { Injectable } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import {User} from '../models/users';

@Injectable({
  providedIn: 'root'
})

export class UsersService {
  currentUser: any;
  userId: any;
  constructor(private afs: AngularFirestore) {
  }


  getUsers(): Observable<User[]> {
    return this.afs.collection('users').valueChanges() as Observable<User[]>;
  }


  getUsertById$(id: string): Observable<User> {
    return this.afs.collection('users').doc(id).valueChanges() as Observable<User>;
  }


}
