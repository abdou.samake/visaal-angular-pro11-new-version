import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {CustomValidators} from 'ngx-custom-validators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  errorMsg: null;
  public form: FormGroup = Object.create(null);
  constructor(private fb: FormBuilder, private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      email: [null, Validators.compose([Validators.required, CustomValidators.email])],
      password: [null, Validators.compose([Validators.required])]
    });
  }

  onSubmit(): void {
    this.errorMsg = null;

    this.authService.signIn(this.form.value.email, this.form.value.password)
      .then(() => {
        this.router.navigate(['dashboards/dashboard1']);
      })
      .catch(err => this.errorMsg = err);
  }

}
