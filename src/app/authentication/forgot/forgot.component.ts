import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';
import { CustomValidators } from 'ngx-custom-validators';
import {AngularFireAuth} from '@angular/fire/auth';


@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss']
})
export class ForgotComponent implements OnInit {
  public form: FormGroup = Object.create(null);
  constructor(private fb: FormBuilder, private router: Router, private afAuth: AngularFireAuth) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      email: [
        null,
        Validators.compose([Validators.required, CustomValidators.email])
      ]
    });
  }

  // tslint:disable-next-line:typedef
  async onSubmit() {
    return await this.afAuth.sendPasswordResetEmail(this.form.value.email)
      .then(() => {
        window.alert('E-mail de réinitialisation du mot de passe envoyé, vérifiez votre boîte de réception.');
        this.router.navigate(['/authentication/login']);
      }).catch((error) => {
        window.alert(error);
      });
  }
}
