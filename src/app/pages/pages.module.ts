
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DemoMaterialModule } from '../demo-material-module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { PagesRoutes } from './pages.routing';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import {MatListModule} from '@angular/material/list';
import { MatIconComponent } from './material-icons/mat-icon.component';
import { TimelineComponent } from './timeline/timeline.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { PricingComponent } from './pricing/pricing.component';
import { HelperComponent } from './helper-classes/helper.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ViewArchivesFilesComponent } from './view-archives-files/view-archives-files.component';
import {PdfViewerModule} from 'ng2-pdf-viewer';
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(PagesRoutes),
        DemoMaterialModule,
        FlexLayoutModule,
        FormsModule,
        MatListModule,
        ReactiveFormsModule,
        NgxDatatableModule,
        PdfViewerModule
    ],
  declarations: [
    MatIconComponent,
    TimelineComponent,
    InvoiceComponent,
    PricingComponent,
    HelperComponent,
    UserProfileComponent,
    ViewArchivesFilesComponent
  ]
})
export class PagesModule {}
