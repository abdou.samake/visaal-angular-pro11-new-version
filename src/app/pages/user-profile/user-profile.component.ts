import {Component, OnDestroy, OnInit} from '@angular/core';
import {PatientsService} from '../../services/patients.service';
import {ActivatedRoute} from '@angular/router';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFireStorage} from '@angular/fire/storage';
import {AngularFirestore} from '@angular/fire/firestore';
import {Observable, Subject} from 'rxjs';
import {PatientModel} from '../../models/patients';
import {User} from '../../models/users';
import {finalize, takeUntil, tap} from 'rxjs/operators';
import {UsersService} from '../../services/users.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit, OnDestroy{

  destroy$: Subject<boolean> = new Subject();

  patient: PatientModel | undefined;

  users: User | undefined;
  firstName: string | undefined;
  lastName: string | undefined;

  userSub: any;

  uid: any;

  currentUser: User | undefined;

  user: any;

  photoUrl: string | undefined;

  downloadURL: Observable<string> | undefined;

  role: any;

  currentUserUid: any;

  constructor(private patientService: PatientsService, private route: ActivatedRoute,
              private afAuth: AngularFireAuth, private userService: UsersService,
              private storage: AngularFireStorage, private afs: AngularFirestore) {
  }

  ngOnInit(): void {

    this.afAuth.authState.subscribe(user => {
      this.user = user;
      this.currentUserUid = this.user.uid;
      this.userSub = this.userService.getUsertById$(this.currentUserUid)
        .pipe(
          takeUntil(this.destroy$),
          tap(
            (data) => {
              this.currentUser = data;
              this.firstName = this.currentUser.firstName;
              this.lastName = this.currentUser.lastName;
              this.photoUrl = this.currentUser.photoUrl;
              this.role = this.currentUser.role;
            }
          )
        ).subscribe();

      this.userService.getUsertById$(this.currentUserUid);
    });

  }


  onFileSelected(event: any): void {
    const n = Date.now();
    const file = event.target.files[0];
    const filePath = `visaalImages/${n}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(`visaalImages/${n}`, file);
    task
      .snapshotChanges()
      .pipe(
        takeUntil(this.destroy$),
          finalize(() => {
            this.downloadURL = fileRef.getDownloadURL();
            this.downloadURL.pipe(
              takeUntil(this.destroy$),
              tap(url => {
                if (url) {
                  this.photoUrl = url;
                }
              })
            )
              .subscribe();
          })
        )
        .subscribe(url => console.log(url));
  }

  setUserPhoto(): void {
    this.afs.collection('users').doc(this.currentUserUid)
      .update({photoUrl: this.photoUrl });
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
