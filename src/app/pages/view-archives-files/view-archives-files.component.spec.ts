import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewArchivesFilesComponent } from './view-archives-files.component';

describe('ViewArchivesFilesComponent', () => {
  let component: ViewArchivesFilesComponent;
  let fixture: ComponentFixture<ViewArchivesFilesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewArchivesFilesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewArchivesFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
