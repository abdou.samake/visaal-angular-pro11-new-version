import {Component, OnDestroy, OnInit} from '@angular/core';
import {PatientsService} from '../../services/patients.service';
import {takeUntil, tap} from 'rxjs/operators';
import {FichierPdf} from '../../models/fichier-pdf';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-view-archives-files',
  templateUrl: './view-archives-files.component.html',
  styleUrls: ['./view-archives-files.component.scss']
})
export class ViewArchivesFilesComponent implements OnInit, OnDestroy {

  destroy$: Subject<boolean> = new Subject();
  files: any;
  srcFilesPdf: FichierPdf[] = [];

  patients: FichierPdf[] = [];

  constructor(private patientServices: PatientsService) { }

  ngOnInit(): void {
    this.patientServices.getAllPatientsFiles$()
      .pipe(
        takeUntil(this.destroy$),
        tap(
          (files: FichierPdf[]) => {
            files.map(file => this.srcFilesPdf.push(file));
            console.log(this.srcFilesPdf);
          }
        )

      )
      .subscribe();
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
