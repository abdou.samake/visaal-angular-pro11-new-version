import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {BreakpointObserver} from '@angular/cdk/layout';
import {PatientsService} from '../../services/patients.service';
import {Router} from '@angular/router';
import {Subject} from 'rxjs';
import {takeUntil, tap} from 'rxjs/operators';

export interface Transaction {
  item: string;
  cost: number;
}

@Component({
  selector: 'app-footerrowtable',
  templateUrl: './footerrow-table.component.html',
  styleUrls: ['./footerrow-table.component.scss']
})

export class FooterrowTableComponent implements OnInit, OnDestroy {

  destroy$: Subject<boolean> = new Subject();

  constructor(breakpointObserver: BreakpointObserver, private patientsService: PatientsService, private router: Router) {
    breakpointObserver.observe(['(width: 300px)']).subscribe(result => {
      this.displayedColumns = result.matches ?
        ['lastName', 'firstName', 'action'] : ['lastName', 'firstName', 'action'];
    });
  }
  displayedColumns = ['lastName', 'firstName', 'action'];
  dataSourceDM1 = new MatTableDataSource();

  dataSourceDM2 = new MatTableDataSource();

  dataSourceDM3 = new MatTableDataSource();

  /*applyFilter(filterValue: string): void {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }*/


  getAllPatientsDM1(): void {
    this.patientsService.getAllPatientUseDM2$()
      .pipe(
        takeUntil(this.destroy$),
        tap(
          res => this.dataSourceDM2.data = res
        )
      )
      .subscribe();
  }

  getAllPatientsDM2(): void {
    this.patientsService.getAllPatientUseDM1$()
      .pipe(
        takeUntil(this.destroy$),
        tap(
          res => this.dataSourceDM1.data = res
        )
      )
      .subscribe();
  }

  getAllPatientsDM3(): void {
    this.patientsService.getAllPatientUseDM3$()
      .pipe(
        takeUntil(this.destroy$),
        tap(
          res => this.dataSourceDM3.data = res
        )
      )
      .subscribe();
  }

  ngOnInit(): void {
    this.getAllPatientsDM1();
    this.getAllPatientsDM2();
    this.getAllPatientsDM3();
  }

  showPatient(): void {
    this.router.navigate(['/forms/editPatient']);
  }
  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
