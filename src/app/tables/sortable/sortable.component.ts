import {Component, ViewChild, AfterViewInit, OnInit} from '@angular/core';

import {DevicesService} from '../../services/devices.service';
import {DeviceModel} from '../../models/devices';
import {finalize, takeUntil, tap} from 'rxjs/operators';
import html2canvas from 'html2canvas';
import jspdf from 'jspdf';
import {AngularFireStorage} from '@angular/fire/storage';
import {Observable, Subject} from 'rxjs';


@Component({
    selector: 'app-sortable',
    templateUrl: './sortable.component.html',
    styleUrls: ['./sortable.component.scss']
})
export class SortableComponent implements OnInit {

  device: DeviceModel | undefined;
  number: string | undefined;
  status: string | undefined = '';
  service: string | undefined = '';

  destroy$: Subject<boolean> = new Subject();
  downloadURL: Observable<string> | undefined;
  filePdfUrl: string | undefined;

    constructor(private devicesService: DevicesService, private storage: AngularFireStorage ) {}

    ngOnInit(): void {
      this.devicesService.getDeviceById('4lHfkrpGTWx3r3CAAQOs')
        .pipe(
          tap(
            (res) => {
              this.device = res;
              this.number = this.device.number;
              this.status = this.device.status;
              this.service = this.device.service;
              console.log(this.device);
            }
          )
        )
        .subscribe();
    }

  onFileSelected(event: any): void {
    const n = Date.now();
    const file = event.target.files[0];
    const filePath = `infoDMpdf/${n}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(`infoDMpdf/${n}`, file);
    task
      .snapshotChanges()
      .pipe(
        takeUntil(this.destroy$),
        finalize(() => {
          this.downloadURL = fileRef.getDownloadURL();
          this.downloadURL.pipe(
            takeUntil(this.destroy$),
            tap(url => {
              if (url) {
                this.filePdfUrl = url;
              }
            })
          )
            .subscribe();
        })
      )
      .subscribe(url => console.log(url));
  }

  generatefolderPDF(): void {
    const data = document.getElementById('contentToConvert');
    if (data) {
      html2canvas(data).then(canvas => {
        const imgWidth = 208;
        const imgHeight = canvas.height * imgWidth / canvas.width;
        const contentDataURL = canvas.toDataURL('image/png');
        const pdf = new jspdf('p', 'mm', 'a4');
        const position = 0;
        pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
        pdf.save(`info-DM.pdf`);
      });
    }
  }

  sendFilePdf() {
      this.devicesService.addFilePdfToDeviceById('4lHfkrpGTWx3r3CAAQOs', {filePdfUrl: this.filePdfUrl});
  }

}

