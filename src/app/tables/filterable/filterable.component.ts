import {Component, OnInit} from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { BreakpointObserver } from '@angular/cdk/layout';
import {PatientModel} from '../../models/patients';
import {PatientsService} from '../../services/patients.service';


@Component({
  selector: 'app-basictable',
  templateUrl: './filterable.component.html',
  styleUrls: ['./filterable.component.scss']
})

export class FilterableComponent implements OnInit{


  patients: PatientModel[] | undefined;

  constructor(breakpointObserver: BreakpointObserver, private patientsService: PatientsService) {
    breakpointObserver.observe(['(max-width: 600px)']).subscribe(result => {
      this.displayedColumns = result.matches ?
        ['sex', 'lastName', 'firstName', 'service', 'dateOfBirthday', 'numberOfSecu',
          'floor', 'numberOfRoom', 'numberOfBed', 'numberOfDevice'] :
        ['sex', 'lastName', 'firstName', 'service', 'dateOfBirthday', 'numberOfSecu',
          'floor', 'numberOfRoom', 'numberOfBed', 'numberOfDevice'];
    });
  }

  displayedColumns = ['sex', 'lastName', 'firstName', 'service', 'dateOfBirthday', 'numberOfSecu',
    'floor', 'numberOfRoom', 'numberOfBed', 'numberOfDevice'];

  dataSource = new MatTableDataSource();

  applyFilter(filterValue: string): void {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  getAllPatients(): void {
    this.patientsService.getAllPatients$().subscribe(res => this.dataSource.data = res);
  }

  ngOnInit(): void {
    this.getAllPatients();
  }

}

