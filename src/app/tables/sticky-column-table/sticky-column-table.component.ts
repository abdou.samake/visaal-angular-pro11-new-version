import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {PatientsService} from '../../services/patients.service';
import {PatientModel} from '../../models/patients';
import {finalize, switchMap, takeUntil, tap} from 'rxjs/operators';
import {combineLatest, Observable, Subject} from 'rxjs';
import {UrinesModel} from '../../models/urines';
import {MatTableDataSource} from '@angular/material/table';
import {OmsDataService} from '../../services/oms-data.service';
import html2canvas from 'html2canvas';
import jspdf from 'jspdf';
import {AngularFireStorage} from '@angular/fire/storage';
import {FormBuilder, FormGroup} from '@angular/forms';
import {DatePipe} from '@angular/common';
import {VolumeAbout24h} from '../../models/volume-about24h';



@Component({
  selector: 'app-sticky-column',
  templateUrl: './sticky-column-table.component.html',
  styleUrls: ['./sticky-column-table.component.scss']
})

export class StickycolumnTableComponent implements OnInit, OnDestroy{

  @ViewChild('fileUploader') fileUploader: any;

  message: string = ' ';

  photoUrl: string | undefined;

  downloadURL: Observable<string> | undefined;

  destroy$: Subject<boolean> = new Subject();

  patient: PatientModel | undefined;

  numberOfTimesUrinated: any;

  urinesData: UrinesModel[] = [];

  tab: any[] = [];

  sum: number | undefined;

  volume: number | undefined;
  myDate: Date = new Date();

  firstName: string;
  lastName: string;

  patients: PatientModel[] | undefined;

  lengthUrine: number | undefined;

  volumeUrineArray: any;

  urineArray: UrinesModel | undefined;

  tabNumber: any[] | undefined;

  displayedColumns =
    ['date', 'hour', 'volumeUrine'];

  dataSource = new MatTableDataSource();

  displayedColumns2 = ['age', 'quantite', 'action'];

  dataOms: any;
  dataSource2 = new MatTableDataSource();

  dataSourceH24 = new MatTableDataSource();

  displayedColumnsh24 = ['jour', 'volume', 'nbrUrine'];

  dateForm: FormGroup;

  maDate: any = new Date();

  totalVolumInCurrentDate: number;
  dateAndVolum: VolumeAbout24h;

  volumeDay: number;

  date: string;

  numberOfTimesSinceTheStartOfUrination: number;

  constructor(private router: Router,
              private patientService: PatientsService,
              private route: ActivatedRoute,
              private omsDataService: OmsDataService,
              private storage: AngularFireStorage,
              private afs: AngularFireStorage,
              private datePipe: DatePipe,
              private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.dateForm = this.fb.group({
      dateOfDay: [null]
    });

    const dataId = this.route.snapshot.params.id;
    this.patientService.getPatientUseDM1ById$(dataId)
      .pipe(
        takeUntil(this.destroy$),
        tap(
          patientGet => {
            takeUntil(this.destroy$);
            this.firstName = patientGet.firstName;
            this.lastName = patientGet.lastName;
          }
        )
      ).subscribe();

    this.getOmsData();
    this.displayAllUnesByPatient();
    this.ngOMSData();
    this.ngSubmitForm();
    this.getSumVolumH24();
  }

  getSumVolumH24(): void {
    this.patientService.getVolumeAndDateDM1ForWhich24Hours()
      .pipe(
        takeUntil(this.destroy$),
        tap(
          (dateAnVolum: any) => {
            this.dataSourceH24 = dateAnVolum;
          }
        )
      ).subscribe();
  }

  displayAllUnesByPatient(): void {
    this.patientService.getAllPatientUseDM1$().subscribe(allData => this.patients = allData);
    const id = this.route.snapshot.params.id;
    this.patientService.getPatientUseDM1ById$(id)
      .pipe(
        takeUntil(this.destroy$),
        tap((data) => this.patient = data),
        switchMap(
          (patient: PatientModel) => {
            return combineLatest(patient.urinesDM.map((urineId) => this.patientService
              .getUrineDataUseDMById(urineId)))
              .pipe(

                tap((urines) => {
                  this.urinesData = urines;
                  this.volumeUrineArray = this.urinesData.reduce((acc: string | any[],
                                                                  currentUrine: { volumeUrine: string; }) =>
                    acc.concat((currentUrine.volumeUrine)), []);
                  this.tabNumber = this.volumeUrineArray?.map((x: any) => {
                    return parseFloat(String(x));
                  });
                  this.sum = this.tabNumber?.reduce((acc: any, currentVolume: any) => acc + currentVolume, 0);

                  /*** calculate the number of times to urinate ***/
                  this.urinesData.map((data: { volumeUrine: any; }) => {
                    return this.tab.push(data.volumeUrine);
                  });
                  this.numberOfTimesSinceTheStartOfUrination = this.urinesData.length;
                })
              );
          }
        ),

        tap((urines: UrinesModel[]) => {
          this.dataSource.data = urines;
          urines.forEach(data => {
            this.urineArray = data;
          });
        }),


      ).subscribe();
  }

  ngSubmitForm(): void {
    this.myDate = this.dateForm.value.dateOfDay;
    this.maDate = this.datePipe.transform(this.myDate, 'dd/MM/yyyy');
    const id = this.route.snapshot.params.id;
    this.patientService.getPatientUseDM1ById$(id)
      .pipe(
        takeUntil(this.destroy$),
        tap((data) => this.patient = data),
        switchMap(
          (patient: PatientModel) => {
            return combineLatest(patient.urinesDM.map((urineId) => this.patientService
              .getUrineDataUseDMById(urineId))).pipe(
              tap((urines: UrinesModel[]) => {
                this.urinesData = urines;
                const result1 = urines.filter((urine) => urine.date === this.maDate);
                this.totalVolumInCurrentDate = result1.reduce((acc, currentVolume) => acc + parseFloat(String(currentVolume.volumeUrine)), 0);
                this.dateAndVolum = {
                  nbrUrine: result1.length,
                  volume: this.totalVolumInCurrentDate,
                  date: this.maDate
                };
                this.patientService.updateVolumeDM1ForWhich24hours(this.dateAndVolum.volume, this.dateAndVolum.date, this.dateAndVolum.nbrUrine);
                this.patientService.getvolumeAndDateOfDayByIdDM1('CZaujeT0XivL9pJ6r3xM').subscribe(
                  (obj) => {
                    this.volumeDay = obj.volume;
                    this.date = obj.date;
                  }
                );

              })
            );
          }
        )

      ).subscribe();
  }

  ngOMSData(): void {
    const dataId = this.route.snapshot.params.id;
    this.omsDataService.getOmsById$(dataId)
      .pipe(
        takeUntil(this.destroy$),
        tap(res => this.dataOms = res)
      ).subscribe();
  }

  getOmsData(): void {
    this.omsDataService.getOmsData$()
      .pipe(
        takeUntil(this.destroy$),
        tap((res: any)  => this.dataSource2 = res)
      )
      .subscribe();
  }

  // tslint:disable-next-line:typedef
  generatePDF(): void {
    const data = document.getElementById('contentToConvert');
    if (data) {
      html2canvas(data).then(canvas => {
        const imgWidth = 208;
        const imgHeight = canvas.height * imgWidth / canvas.width;
        const contentDataURL = canvas.toDataURL('image/png');
        const pdf = new jspdf('p', 'mm', 'a4');
        const position = 0;
        pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
        pdf.save(`données_du_patient ${this.lastName} ${this.firstName}.pdf`);
      });
    }
  }

  onFileSelected(event: any): void {
    const n = Date.now();
    const file = event.target.files[0];
    const filePath = `filesDataPatients/${n}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(`filesDataPatients/${n}`, file);
    task
      .snapshotChanges()
      .pipe(
        takeUntil(this.destroy$),
        finalize(() => {
          this.downloadURL = fileRef.getDownloadURL();
          this.downloadURL.pipe(
            takeUntil(this.destroy$),
            tap(url => {
              if (url) {
                this.photoUrl = url;
              }
            })
          )
            .subscribe();
        })
      )
      .subscribe(url => console.log(url));
  }

  sendFileToDatabase() {
    return this.patientService.addFile({
      firstName: this.firstName,
      lastName: this.lastName,
      fichierPdf: this.photoUrl
    }).then(() => {
      this.fileUploader.nativeElement.value = null;
      this.message = 'le fichier a été bien envoyé';
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
