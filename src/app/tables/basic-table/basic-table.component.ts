import { Component } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { BreakpointObserver } from '@angular/cdk/layout';
import {PatientsService} from '../../services/patients.service';
import {Router} from '@angular/router';
import {DevicesService} from '../../services/devices.service';

@Component({
    selector: 'app-basictable',
    templateUrl: './basic-table.component.html',
    styleUrls: ['./basic-table.component.scss']
})
export class BasicTableComponent {

  totalDm: any;

  deviceStatusPris: any;

  restDMdispo: any;

  restInDMdispo: any;

  restMaintenance: any;

  tab: Array<any> = [];

  constructor(breakpointObserver: BreakpointObserver, private devicesService: DevicesService, private router: Router) {
    breakpointObserver.observe(['(width: 300px)']).subscribe(result => {
      this.displayedColumns = result.matches ?
        ['number', 'service', 'status'] : ['number', 'service', 'status', 'action'];
    });
  }

  displayedColumns = ['number', 'service', 'status', 'action'];
  dataSource = new MatTableDataSource();


  applyFilter(filterValue: string): void {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  getAllDevices(): void {
    this.devicesService.getAllDevices$().subscribe(res => this.dataSource.data = res);
  }

  ngOnInit(): void {
    this.getAllDevices();

    this.devicesService.getAllDevices$().subscribe(
      (devices) => {
        const tabDevice = devices.map((device: any) => this.tab.push(device));
        this.totalDm = tabDevice.length;
      }
    );

    this.devicesService.getAllDevices$().subscribe(
      (devices) => {
        this.deviceStatusPris = devices.filter((device) => device.status === 'Libre');
        this.restDMdispo = this.deviceStatusPris.length;
      }
    );

    this.devicesService.getAllDevices$().subscribe(
      (devices) => {
        this.deviceStatusPris = devices.filter((device) => device.status === 'Attribué');
        this.restInDMdispo = this.deviceStatusPris.length;
      }
    );

    this.devicesService.getAllDevices$().subscribe(
      (devices) => {
        this.deviceStatusPris = devices.filter((device) => device.status === 'Maintenance');
        this.restMaintenance = this.deviceStatusPris.length;
      }
    );

  }

  onclickSwitchDeviceStatus(id: string | undefined, status: any): void {
    this.devicesService.switchAllDeviceStatus(id, status)
      .then(() => console.log('succes'));
  }

  showPatient(): void {
    this.router.navigate(['/forms/editPatient']);
  }

  onclickSwitchDeviceMaintenance(id: any, status: string): void {
    this.devicesService.switchAllDeviceStatus(id, 'Maintenace')
      .then(() => console.log('succes'));
  }
}
