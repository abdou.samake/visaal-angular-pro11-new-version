import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StickyColumnTableDM3Component } from './sticky-column-table-dm3.component';

describe('StickyColumnTableDM3Component', () => {
  let component: StickyColumnTableDM3Component;
  let fixture: ComponentFixture<StickyColumnTableDM3Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StickyColumnTableDM3Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StickyColumnTableDM3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
