import {ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {combineLatest, Observable, Subject} from 'rxjs';
import {PatientModel} from '../../models/patients';
import {UrinesModel} from '../../models/urines';
import {MatTableDataSource} from '@angular/material/table';
import {ActivatedRoute, Router} from '@angular/router';
import {PatientsService} from '../../services/patients.service';
import {OmsDataService} from '../../services/oms-data.service';
import {finalize, switchMap, takeUntil, tap} from 'rxjs/operators';
import html2canvas from 'html2canvas';
import jspdf from 'jspdf';
import {AngularFireStorage} from '@angular/fire/storage';
import {DatePipe} from '@angular/common';
import {VolumeAbout24h} from '../../models/volume-about24h';
import {FormBuilder, FormGroup} from '@angular/forms';


@Component({
  selector: 'app-sticky-column-table-dm3',
  templateUrl: './sticky-column-table-dm3.component.html',
  styleUrls: ['./sticky-column-table-dm3.component.scss']
})

export class StickyColumnTableDM3Component implements OnInit{

  @ViewChild('fileUploader') fileUploader: any;

  dateForm: FormGroup;

  fileUrl: string;

  downloadURL: Observable<string> | undefined;

  destroy$: Subject<boolean> = new Subject();

  numberOfTimesSinceTheStartOfUrination: number;

  patient: PatientModel;

  urinesData: UrinesModel[] = [];

  tab: any[] = [];

  sum = 0;

  myDate: any;

  firstName: string;

  lastName: string;

  time;

  maDate: any = new Date();

  patients: PatientModel[];

  patientEdit: any;

  volumeUrineArray: any;

  urineArray: UrinesModel;

  tabNumber: any;

  message = '';

  displayedColumns =

    ['date', 'hour', 'volumeUrine'];

  dataSource = new MatTableDataSource();

  displayedColumns2 = ['age', 'quantite', 'action'];

  dataSource2 = new MatTableDataSource();

  dataOms: any;

  DMUrine: any;

  dataSourceH24 = new MatTableDataSource();

  dateAndVolum: VolumeAbout24h;

  displayedColumnsh24 = ['jour', 'volume', 'nbrUrine'];

  totalVolumInCurrentDate: number;

  volumeDay: number;

  date: string;

  constructor(private router: Router,
              private patientService: PatientsService,
              private route: ActivatedRoute,
              private omsDataService: OmsDataService,
              private storage: AngularFireStorage,
              private cf: ChangeDetectorRef,
              private datePipe: DatePipe,
              private fb: FormBuilder) {
    const d = new Date();
    this.time = d.getTime();
    this.maDate = this.datePipe.transform(this.maDate, 'dd/MM/yyyy');
  }

  ngOnInit(): void {
    this.dateForm = this.fb.group({
      dateOfDay: [null]
    });

    const patientId = this.route.snapshot.params.id;
    this.patientService.getPatientUseDM3ById$(patientId)
      .pipe(
        takeUntil(this.destroy$),
        tap((patient) => {
            this.lastName = patient.lastName;
            this.firstName = patient.firstName;
            console.log(this.lastName);
            console.log(this.firstName);
          }
          )
      )
      .subscribe();

    this.ngSubmitForm();
    this.ngOMSData();
    this.displayAllUnesByPatient();
    this.getOmsData();
    this.getSumVolumH24();
  }

  ngOMSData(): void {
    const dataId = this.route.snapshot.params.id;
    this.omsDataService.getOmsById$(dataId)
      .pipe(
        takeUntil(this.destroy$),
        tap(res => this.dataOms = res)
      ).subscribe();
  }

  resetData(): void  {
      const idnum = this.route.snapshot.params.id;
      this.patientService.updatePatientDM3ById$(this.patientEdit, idnum);
  }

  getOmsData(): void {
    this.omsDataService.getOmsData$()
      .pipe(
        takeUntil(this.destroy$),
        tap((res: any)  => this.dataSource2 = res)
      )
      .subscribe();
  }

  getSumVolumH24(): void {
    this.patientService.getVolumeAndDateDM3ForWhich24Hours()
      .pipe(
        takeUntil(this.destroy$),
        tap(
          (dateAnVolum: any) => {
            this.dataSourceH24 = dateAnVolum;
          }
        )
      ).subscribe();
  }

  // tslint:disable-next-line:typedef
  generatePDF(): void {
    const data = document.getElementById('contentToConvert');
    if (data) {
      html2canvas(data).then(canvas => {
        const imgWidth = 208;
        const imgHeight = canvas.height * imgWidth / canvas.width;
        const contentDataURL = canvas.toDataURL('image/png');
        const pdf = new jspdf('p', 'mm', 'a4');
        const position = 0;
        pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
        pdf.save(`données_du_patient ${this.lastName} ${this.firstName}.pdf`);
      });
    }
  }

  onFileSelected(event: any): void {
    const n = Date.now();
    const file = event.target.files[0];
    const filePath = `filesDataPatients/${n}`;
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(`filesDataPatients/${n}`, file);
    task
      .snapshotChanges()
      .pipe(
        takeUntil(this.destroy$),
        finalize(() => {
          this.downloadURL = fileRef.getDownloadURL();
          this.downloadURL.pipe(
            takeUntil(this.destroy$),
            tap(url => {
              if (url) {
                this.fileUrl = url;
              }
            })
          )
            .subscribe();
        })
      )
      .subscribe();
  }

  sendFileToDatabase() {
    return this.patientService.addFile({
      firstName: this.firstName,
      lastName: this.lastName,
      fichierPdf: this.fileUrl
    }).then(() => {
      this.fileUploader.nativeElement.value = null;
      this.message = 'le fichier a été bien envoyé !';
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  displayAllUnesByPatient(): void {
    this.patientService.getAllPatientUseDM3$().subscribe(allData => this.patients = allData);
    const id = this.route.snapshot.params.id;
    this.patientService.getPatientUseDM3ById$(id)
      .pipe(
        takeUntil(this.destroy$),
        tap((data) => this.patient = data),
        switchMap(
          (patient: PatientModel) => {
            return combineLatest(patient.urinesDM.map((urineId) => this.patientService
              .getUrineDataUseDMById(urineId)))
              .pipe(

                tap((urines) => {
                  this.urinesData = urines;
                  this.volumeUrineArray = this.urinesData.reduce((acc: string | any[],
                                                                  currentUrine: { volumeUrine: string; }) =>
                    acc.concat((currentUrine.volumeUrine)), []);
                  this.tabNumber = this.volumeUrineArray?.map((x: any) => {
                    return parseFloat(String(x));
                  });
                  this.sum = this.tabNumber?.reduce((acc: any, currentVolume: any) => acc + currentVolume, 0);

                  /*** calculate the number of times to urinate ***/
                  this.urinesData.map((data: { volumeUrine: any; }) => {
                    return this.tab.push(data.volumeUrine);
                  });
                  this.numberOfTimesSinceTheStartOfUrination = this.urinesData.length;
                })
            );
          }
        ),

        tap((urines: UrinesModel[]) => {
          this.dataSource.data = urines;
          urines.forEach(data => {
            this.urineArray = data;
          });
        }),


      ).subscribe();
  }

  ngSubmitForm(): void {
    this.myDate = this.dateForm.value.dateOfDay;
    this.maDate = this.datePipe.transform(this.myDate, 'dd/MM/yyyy');
    const id = this.route.snapshot.params.id;
    this.patientService.getPatientUseDM3ById$(id)
      .pipe(
        takeUntil(this.destroy$),
        tap((data) => this.patient = data),
        switchMap(
          (patient: PatientModel) => {
            return combineLatest(patient.urinesDM.map((urineId) => this.patientService
              .getUrineDataUseDMById(urineId))).pipe(
              tap((urines: UrinesModel[]) => {
                this.urinesData = urines;
                const result1 = urines.filter((urine) => urine.date === this.maDate);
                this.totalVolumInCurrentDate = result1.reduce((acc, currentVolume) => acc + parseFloat(String(currentVolume.volumeUrine)), 0);
                this.dateAndVolum = {
                  nbrUrine: result1.length,
                  volume: this.totalVolumInCurrentDate,
                  date: this.maDate
                };
                this.patientService.updateVolumeDM3ForWhich24hours(this.dateAndVolum.volume, this.dateAndVolum.date, this.dateAndVolum.nbrUrine);
                this.patientService.getvolumeAndDateOfDayByIdDM3('R32fQzBOQYSKwkubFWGx').subscribe(
                  (obj) => {
                    this.volumeDay = obj.volume;
                    this.date = obj.date;
                  }
                );

              })
            );
          }
        )

      ).subscribe();
  }

}
