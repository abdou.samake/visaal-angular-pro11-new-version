import { Routes } from '@angular/router';
import { FullComponent } from './layouts/full/full.component';
import { AppBlankComponent } from './layouts/blank/blank.component';
import {LoginComponent} from './authentication/login/login.component';
import {AngularFireAuthGuard} from '@angular/fire/auth-guard';

export const AppRoutes: Routes = [
      {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full'
      },
      {
        path: 'material',
        loadChildren: () => import('./material-component/material.module').then(m => m.MaterialComponentsModule)
  },
  {
    path: 'login',
    component: LoginComponent
  },
    {
        path: '',
        component: FullComponent,
        children: [
          {
            path: '',
            redirectTo: '/login',
            pathMatch: 'full'
          },

            {
                path: 'dashboards',
                loadChildren: () => import('./dashboards/dashboards.module').then(m => m.DashboardsModule),
              canActivate: [AngularFireAuthGuard]
            },
            {
                path: 'material',
                loadChildren: () => import('./material-component/material.module').then(m => m.MaterialComponentsModule),
              canActivate: [AngularFireAuthGuard]
            },
            {
                path: 'apps',
                loadChildren: () => import('./apps/apps.module').then(m => m.AppsModule),
              canActivate: [AngularFireAuthGuard]
            },
            {
                path: 'forms',
                loadChildren: () => import('./forms/forms.module').then(m => m.FormModule),
              canActivate: [AngularFireAuthGuard]
            },
            {
                path: 'tables',
                loadChildren: () => import('./tables/tables.module').then(m => m.TablesModule),
              canActivate: [AngularFireAuthGuard]
            },
            {
                path: 'tree',
                loadChildren: () => import('./tree/tree.module').then(m => m.TreeModule),
              canActivate: [AngularFireAuthGuard]
            },
            {
                path: 'datatables',
                loadChildren: () => import('./datatables/datatables.module').then(m => m.DataTablesModule),
              canActivate: [AngularFireAuthGuard]
            },
            {
                path: 'pages',
                loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule),
              canActivate: [AngularFireAuthGuard]
            },
            {
                path: 'widgets',
                loadChildren: () => import('./widgets/widgets.module').then(m => m.WidgetsModule),
              canActivate: [AngularFireAuthGuard]
            },
            {
                path: 'charts',
                loadChildren: () => import('./charts/chartslib.module').then(m => m.ChartslibModule),
              canActivate: [AngularFireAuthGuard]
            },
            {
                path: 'multi',
                loadChildren: () => import('./multi-dropdown/multi-dd.module').then(m => m.MultiModule),
              canActivate: [AngularFireAuthGuard]
            }
        ]
    },
    {
        path: '',
        component: AppBlankComponent,
        children: [
            {
                path: 'authentication',
                loadChildren:
                    () => import('./authentication/authentication.module').then(m => m.AuthenticationModule)
            }
        ]
    },
    {
        path: '**',
        redirectTo: 'authentication/404'
    }
];
