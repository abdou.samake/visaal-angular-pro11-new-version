import {Component, OnInit} from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {MessageService} from './services/message.service';
import {AngularFireMessaging} from '@angular/fire/messaging';
import {UsersService} from './services/users.service';
import {AngularFireAuth} from '@angular/fire/auth';
import {User} from './models/users';
import {AuthGuard} from './shared/auth.guard';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  message: any;
  userId: any;
  constructor(private messagingService: MessageService, private angularFireMessaging: AngularFireMessaging,
              private userService: UsersService, private afAuth: AngularFireAuth, private authGard: AuthGuard) {}

  ngOnInit(): void {
    this.messagingService.requestPermission();
    this.messagingService.receiveMessage();
    this.message = this.messagingService.currentMessage;


    /*this.afAuth.authState.subscribe(
      (user: any) => {
        this.userId = user.uid;
        console.log(this.userId);
        this.userService.getUsertById$(this.userId).subscribe(
          (currentUser: User) => console.log(currentUser.role)
        );
      }
    );*/

    console.log(this.authGard.userRole);
  }

}
