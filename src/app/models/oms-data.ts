export interface OmsData {
  id: string;
  age: string;
  volume: string;
}
