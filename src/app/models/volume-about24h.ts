export interface VolumeAbout24h {
  volume: number;
  nbrUrine: number;
  date: string;
}
