import {UrinesModel} from './urines';

export interface PatientModel {
  id?: string;
  lastName: string;
  firstName: string;
  service: string;
  numberOfRoom: number;
  numberOfBed: number;
  floor: number;
  sex: string;
  stateOfRinsingLiquid: string;
  stateOfUrinaryCollector: string;
  dateOfBirthday: number;
  numberOfDevice: string;
  numberOfSecu: string;
  date: number;
  amountOfUrine: null;
  hour: null;
  duration: number;
  ph: null;
  urines: string[];
  urinesDM: string[];
  urinesH24DM: string[];
  urinesData: UrinesModel[];
  deviceId: string;
}

export enum PatientRolesEnum {
  isPatient = 'isPatient'
}
