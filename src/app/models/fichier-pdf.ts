export interface FichierPdf {
  firstName: string;
  lastName: string;
  fichierPdf: string;
}
