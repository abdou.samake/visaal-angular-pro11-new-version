export interface DeviceModel {
  id?: string;
  status?: string;
  service?: string;
  number?: string;
}
