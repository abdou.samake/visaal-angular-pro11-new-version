import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router} from '@angular/router';
import { Observable } from 'rxjs';
import {UsersService} from '../services/users.service';
import {AngularFireAuth} from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  userId: any;
  userRole: any;

  constructor(private userService: UsersService, private afAuth: AngularFireAuth, private router: Router) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<any | UrlTree> | Promise<any | UrlTree> | any | UrlTree {
    this.afAuth.authState.subscribe(
      (user: any) => this.userId = user.uid
    );
    this.userService.getUsertById$(this.userId).subscribe(
      (currentUser) => {
        if (currentUser.role === 'Soignant') {
          return true;
        }
        else {
          this.router.navigate(['dashboard/dasboard1']);
          return false;
        }
      }
    );
    return true;
  }


}
