import { TestBed } from '@angular/core/testing';

import { AuthSoignantResolver } from './auth-soignant.resolver';

describe('AuthSoignantResolver', () => {
  let resolver: AuthSoignantResolver;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    resolver = TestBed.inject(AuthSoignantResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
